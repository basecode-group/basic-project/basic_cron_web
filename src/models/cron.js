import {getCronList, getCronSelectData, saveCron, getCronById, updateCron, deleteCron} from '@/services/cron'
import {message} from 'antd';

const CronModel = {
  namespace: 'cron',
  state: {
    list: [],
    pagination: {
      pageSize: 10,
      current: 1,
      total: 0,
    },
    loading: false,
    editLoading: false,
    query: {
      pageNum: 1,
      pageSize: 10
    },
    cronSelectData: [],
  },
  effects: {
    // 查询列表
    * queryCronList({params}, {call, put}) {
      yield put({
        type: 'changeLoading',
        payload: true,
      });
      const response = yield call(getCronList, params);
      if (response.code === "000") {
        yield put({
          type: 'setCronList',
          payload: response,
        });
      } else {
        yield put({
          type: 'changeLoading',
          payload: false,
        });
      }
    },

    // 查询定时任务
    * getCronSelectData(_, {call, put}) {
      const response = yield call(getCronSelectData);
      yield put({
        type: 'setCronSelectData',
        payload: response,
      });
    },
    // 保存定时任务
    * saveCron({params}, {call, put}) {
      // 保存
      const response = yield call(saveCron, params);
      if (response.code === '000') {
        message.success('新建定时任务成功')
        yield put({type: 'queryCronList', params: {},});
        return response;
      }
    },
    // 保存定时任务
    * updateCron({params}, {call, put}) {
      // 保存
      const response = yield call(updateCron, params);
      if (response.code === '000') {
        message.success('编辑定时任务成功')
        yield put({
          type: 'queryCronList',
          params: {},
        });
        return response;
      }
    },
    * getCronById({params}, {call, put}) {
      yield put({
        type: 'changeEditLoading',
        payload: true,
      });
      const response = yield call(getCronById, params);
      yield put({
        type: 'changeEditLoading',
        payload: false,
      });
      return response;
    },
    // 删除
    * deleteCron({params}, {call, put}) {
      yield put({
        type: 'changeLoading',
        payload: true,
      });
      const result = yield call(deleteCron, params);
      if (result.code === "000") {
        message.success("删除记录成功");
        yield put({
          type: 'queryCronList',
          params: {},
        });
      } else {
        yield put({
          type: 'changeLoading',
          payload: false,
        });
      }
    }
  },
  reducers: {
    setCronList(state, {payload}) {
      const {result} = payload;
      return {
        ...state,
        loading: false,
        editLoading: false,
        pagination: {
          pageSize: result.size,
          current: result.current,
          total: result.total,
        },
        list: result.records
      };
    },
    changeLoading(state, {payload}) {
      return {...state, loading: payload}
    },
    changeEditLoading(state, {payload}) {
      return {...state, editLoading: payload}
    },
    setCronSelectData(state, {payload}) {
      return {
        ...state, cronSelectData: payload.result
      }
    }
  },
};
export default CronModel;
