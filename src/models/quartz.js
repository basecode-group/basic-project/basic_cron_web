import {
  getQuartzNextDate,
  getSchedule,
  createSchedule,
  pauseSchedule,
  resumeSchedule,
  runOnceSchedule,
  updateSchedule,
  deleteSchedule
} from '@/services/quartz';
import {getCronList} from "@/services/cron";
import { message } from 'antd';

const quartzModel = {
  namespace: 'quartz',
  state: {
    nextDate: [],
    schedule: {},
  },
  effects: {
    // 查询下一期
    *getQuartzNextDate({params}, { call, put }){
      const response = yield call(getQuartzNextDate,params);
      yield put({
        type: 'setQuartzNextDate',
        payload: response,
      });
    },
    // 获取定时任务详情
    * getScheduleDetail({params}, {call, put}) {
      const response = yield call(getSchedule, params);
      if (response.code === '000'){
        yield put({
          type: 'setSchedule',
          result: response.result,
        });
      }
    },
    // 创建定时任务
    * createSchedule({params}, {call, put}) {
      const response = yield call(createSchedule, params);
      if (response.code === '000'){
        message.success('定时任务创建成功');
        return response;
      }
    },
    // 暂停定时任务
    * pauseSchedule({params}, {call, put}) {
      const response = yield call(pauseSchedule, params);
      if (response.code === '000'){
        message.success('定时任务暂停成功');
        return response;
      }
    },
    // 恢复定时任务
    * resumeSchedule({params}, {call, put}) {
      const response = yield call(resumeSchedule, params);
      if (response.code === '000'){
        message.success('定时任务恢复成功');
        return response;
      }
    },
    // 执行一次定时任务
    * runOnceSchedule({params}, {call, put}) {
      const response = yield call(runOnceSchedule, params);
      if (response.code === '000'){
        message.success('定时任务执行成功');
        return response;
      }
    },
    // 更新定时任务
    * updateSchedule({params}, {call, put}) {
      const response = yield call(updateSchedule, params);
      if (response.code === '000'){
        message.success('定时任务更新成功');
        return response;
      }
    },
    // 删除定时任务
    * deleteSchedule({params}, {call, put}) {
      const response = yield call(deleteSchedule, params);
      if (response.code === '000'){
        message.success('定时任务删除成功');
        return response;
      }
    },
  },
  reducers: {
    setQuartzNextDate(state, {payload}){
      const {result} = payload;
      return {
        ...state,
        nextDate: result,
      };
    },
    setSchedule(state, { result }) {
      return {...state, schedule: result };
    }
  }
}

export default quartzModel;
