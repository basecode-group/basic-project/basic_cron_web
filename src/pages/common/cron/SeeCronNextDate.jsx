import React, {useState, useEffect} from 'react';
import {Form, Collapse, List} from 'antd';
import {connect} from 'dva';

const {Panel} = Collapse;

/**
 * 查询 下一期Cron
 * @param props
 * @param ref
 * @returns {*}
 * @constructor
 */
function SeeCronNextDate(props, ref) {

  const { value, quartz, dispatch } = props;

  // 初始化
  useEffect(() => {
    getQuartzNextDate({cron: value !== undefined ? value : '* * * * * ?'})
  }, []);

  const getQuartzNextDate = (params) => {
    if (dispatch) {
      dispatch({
        type: 'quartz/getQuartzNextDate',
        params: params,
      });
    }
  }

  return (
    <div>
      <Collapse style={{marginTop: 10}} defaultActiveKey={['1']}>
        <Panel header="近5次执行时间" key="1">
          <List
            bordered
            dataSource={quartz.nextDate}
            renderItem={(item, key) => <List.Item>{`第${key + 1}执行时间：${item}`}</List.Item>}
          />
        </Panel>
      </Collapse>
    </div>
  )
}


const form = Form.create({name: 'SeeCronNextDate',})(SeeCronNextDate)

export default connect(({quartz}) => ({
  quartz: quartz
}))(form);
