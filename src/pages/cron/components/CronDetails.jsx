import React, { useState, useEffect } from 'react';
import {connect} from 'dva';
import { Drawer, Button, Row, Col, Spin, Popconfirm, Icon } from 'antd';
import IconFont from "@/pages/common/icon/IconFont";
import SeeCronNextDate from "@/pages/common/cron/SeeCronNextDate";
import ReactJson from 'react-json-view'
import EditCron from "@/pages/common/cron/EditCron";

/**
 * 定时任务 管理详情
 * @param props
 * @param ref
 * @returns {*}
 * @constructor
 */
function CronDetails(props,ref) {
  const [ visible, setVisible ] = useState(false);
  const { dispatch, loading, cron, children, nextDate } = props;

  // 初始化
  const preInit = () => {
    setVisible(true);
    getQuartzNextDate({cron: cron.cron})
  }

  const getQuartzNextDate = (params) => {
    if (dispatch) {
      dispatch({
        type: 'quartz/getQuartzNextDate',
        params: params,
      });
    }
  }

  // 创建定时任务
  const jobCreate = () => {
    if (dispatch) {
      dispatch({
        type: 'quartz/createSchedule',
        params: { jobName: cron.mark }
      });
    }
  }

  // 暂停定时任务
  const jobPause = () => {
    if (dispatch) {
      dispatch({
        type: 'quartz/pauseSchedule',
        params: { jobName: cron.mark }
      });
    }
  }

  // 恢复定时任务
  const jobResume = () => {
    if (dispatch) {
      dispatch({
        type: 'quartz/resumeSchedule',
        params: { jobName: cron.mark }
      });
    }
  }

  // 执行定时任务
  const jobRun = () => {
    if (dispatch) {
      dispatch({
        type: 'quartz/runOnceSchedule',
        params: { jobName: cron.mark }
      });
    }
  }

  // 删除定时任务
  const jobDelete = () => {
    if (dispatch) {
      dispatch({
        type: 'quartz/deleteSchedule',
        params: { jobName: cron.mark }
      });
    }
  }

  return (
    <span>
      <span onClick={preInit}>
        {
          children ? children : null
        }
      </span>
      <Drawer
        title="定时任务详情"
        placement="right"
        closable={false}
        onClose={() => setVisible(false)}
        visible={visible}
        width={650}
      >
        <Spin spinning={loading}>

          {/* 定时任务详情 */}
          <div>
            <h3>定时任务</h3>
            <Row style={{ marginTop: 30, marginBottom: 20 }}>
              <Col span={24}>
                <IconFont type="icon-biaoji"
                  style={{ fontSize: '1.4em', float: 'left', marginRight: 10 }}
                ></IconFont>
                <DescriptionItem
                  title="定时任务名称"
                  content={
                    <span style={{ color: '#eb2f96' }}>{cron.name} ( {cron.mark} )</span>
                  }
                />
              </Col>
              <Col span={24}>
                <IconFont
                  type="icon-jiandangzhuangtai-copy-copy"
                  style={{ fontSize: '1.6em', float: 'left', marginRight: 10, marginLeft: -2 }}
                ></IconFont>
                <DescriptionItem
                  title="定时任务状态"
                  content={
                    <span style={{ color: '#eb2f96' }}>
                      {cron.tails.cronStatus.label} ( {cron.tails.cronStatus.value} )  【 {cron.tails.currentCron} 】
                    </span>
                  }
                />
              </Col>
              <Col span={24}>
                <IconFont
                  type="icon-zhengzebiaodashi"
                  style={{ fontSize: '1.1em', float: 'left', marginRight: 15, marginLeft: 1, marginTop: 3 }}
                ></IconFont>
                <DescriptionItem
                  title="Cron 表达式 ( 数据库 )"
                  content={
                    <span style={{ color: '#1890FF' }}>
                      <EditCron key={visible} cron={cron.cron} jobName={cron.mark} />
                    </span>
                  }
                  style={{marginTop: -7}}
                />
              </Col>
              <Col span={24}>
                <IconFont
                  type="icon-zhengzebiaodashi"
                  style={{ fontSize: '1.1em', float: 'left', marginRight: 15, marginLeft: 1, marginTop: 3 }}
                ></IconFont>
                <DescriptionItem
                  title="Cron 表达式 （ 原始CRON ）"
                  content={
                    <span style={{ color: '#1890FF' }}>
                      <span style={{ marginLeft: 20, color: '#c41d7f' }}>{cron.tails.oldCron}</span>
                    </span>
                  }
                />
              </Col>
              <Col span={24}>
                {cron.tails.cronStatus.value === 'NORMAL' ? (
                  <ReactJson displayDataTypes={false} src={nextDate} />
                ) : null}
              </Col>
            </Row>
            <h3>操作</h3>
            <Row style={{ marginTop: 20 }}>
            <Col span={4}>
              <Popconfirm
                onConfirm={jobCreate}
                title="确定要创建定时任务吗？"
                icon={<Icon type="question-circle-o" style={{ color: 'red' }} />}
              >
                <Button type="primary" shape="circle" icon="plus" />
                <span style={{ marginLeft: 10 }}>创建</span>
              </Popconfirm>
            </Col>
            <Col span={4}>
              <Popconfirm
                onConfirm={jobPause}
                title="确定要暂停定时任务吗？"
                icon={<Icon type="question-circle-o" style={{ color: 'red' }} />}
              >
                <Button type="primary" shape="circle" icon="pause" />
                <span style={{ marginLeft: 10 }}>暂停</span>
              </Popconfirm>
            </Col>
            <Col span={4}>
              <Popconfirm
                onConfirm={jobResume}
                title="确定要恢复定时任务吗？"
                icon={<Icon type="question-circle-o" style={{ color: 'red' }} />}
              >
                <Button type="primary" shape="circle" icon="caret-right" />
                <span style={{ marginLeft: 10 }}>恢复</span>
              </Popconfirm>
            </Col>
            <Col span={4}>
              <Popconfirm
                onConfirm={jobRun}
                title="确定要执行定时任务吗？"
                icon={<Icon type="question-circle-o" style={{ color: 'red' }} />}
              >
                <Button type="primary" shape="circle" icon="redo" />
                <span style={{ marginLeft: 10 }}>单次执行</span>
              </Popconfirm>
            </Col>
            <Col span={4} offset={1}>
              <Popconfirm
                onConfirm={jobDelete}
                title="确定要删除定时任务吗？"
                icon={<Icon type="question-circle-o" style={{ color: 'red' }} />}
              >
                <Button type="danger" shape="circle" icon="delete" />
                <span style={{ marginLeft: 10 }}>删除</span>
              </Popconfirm>
            </Col>
          </Row>
          </div>
        </Spin>
        {/* 操作 */}
        <div
          style={{
            position: 'absolute',
            right: 0,
            bottom: 0,
            width: '100%',
            borderTop: '1px solid #e9e9e9',
            padding: '10px 16px',
            background: '#fff',
            textAlign: 'right',
          }}
        >
          <Button onClick={() => setVisible(false)} style={{marginRight: 8}}>
            Cancel
          </Button>
        </div>
      </Drawer>
    </span>
  )
}

const DescriptionItem = ({ title, content, style }) => (
  <div
    style={{
      fontSize: 14,
      lineHeight: '22px',
      marginBottom: 0,
      color: 'rgba(0,0,0,0.65)',
      ...style
    }}
  >
    <p
      style={{
        marginRight: 8,
        marginBottom: 13,
        display: 'inline-block',
        color: 'rgba(0,0,0,0.85)',
      }}
    >
      {title}:
    </p>
    {content}
  </div>
);

export default connect(({cron, quartz, loading}) => ({
  loading: loading.models.cron || loading.models.quartz,
  nextDate: quartz.nextDate
}))(CronDetails);
